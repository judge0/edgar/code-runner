const config = require('./config/config');
const winston = require('./config/winston-config');

const EXPOSE_METRICS = !!process.env.METRICS;



class PrometheusMetrics {

    constructor() {
        this.metrics = null;
        console.log("Prometheus metrics constructed.");
    }

    init(app) {

        if (EXPOSE_METRICS) {
            console.log('**********************************************************');
            console.log('********************  Exposing metrics. ******************');
            console.log('**********************************************************');
            var expressResponseSize = require('express-response-size');
            const prometheus = require('prom-client');
            const collectDefaultMetrics = prometheus.collectDefaultMetrics;
            const prefix = 'code_runner_';
            collectDefaultMetrics({
                prefix
            });
            this.metrics = {
                requestCounter: new prometheus.Counter({
                    name: prefix + 'code_run_request_count',
                    help: 'The number of requests for code execution.',
                    labelNames: ['language']
                }),
                outcomeCounter: new prometheus.Counter({
                    name: prefix + 'code_run_outcome_count',
                    help: 'The number of outcomes for code execution',
                    labelNames: ['language', 'outcome_status']
                }),
                httpRequestDuration: new prometheus.Histogram({
                    name: prefix + 'http_request_duration_seconds',
                    help: 'Request duration in seconds',
                    buckets: [0.1, 0.5, 1, 2, 5, 10, 15, 20, 30, 50, 100, 500],
                    labelNames: ['language', 'runner']
                }),
                httpResponseSize: new prometheus.Histogram({
                    name: prefix + 'http_response_size_bytes',
                    help: 'Response size in bytes',
                    buckets: [1000, 5000, 10000, 100000, 200000, 500000],
                }),
            }
            app.use(expressResponseSize((req, res, size) => {
                this.metrics.httpResponseSize.observe(parseInt(size));
            }));

            if (process.env.pm_id !== undefined) {

                console.log('Cluster mode, metrics will be aggregated.');
                const pm2mes = require('pm2-messages');
                pm2mes.onMessage('get_prom_register', (packet) => prometheus.register.getMetricsAsJSON());

                app.get('/metrics', async(req, res) => {
                    const metricsArr = await pm2mes.getMessages('get_prom_register');
                    res.end(prometheus.AggregatorRegistry.aggregate(metricsArr).metrics());
                });

            } else {
                app.get('/metrics', (req, res) => {
                    res.end(prometheus.register.metrics())
                });
            }
        }
    }


    markStartRequest(languageLabel, workerHostLabel) {
        if (EXPOSE_METRICS) {
            this.metrics.requestCounter.labels(languageLabel).inc();
            return this.metrics.httpRequestDuration.labels(languageLabel, workerHostLabel).startTimer();
        } else {
            return null;
        }
    }
    markSubmissionTimeout(languageLabel, timer) {
        this.metrics && this.metrics.outcomeCounter.labels(languageLabel, 'Sbm-Timeout').inc();
        this.markTimeout(languageLabel, timer);
    }
    markWaitingRoomTimeout(languageLabel, timer) {
        this.metrics && this.metrics.outcomeCounter.labels(languageLabel, 'WR-Timeout').inc();
        this.markTimeout(languageLabel, timer);
    }
    markTimeout(languageLabel, timer) {
        this.metrics && this.metrics.outcomeCounter.labels(languageLabel, 'Total Timeout').inc();
        timer && timer();
    }
    markSuccess(languageLabel, timer) {
        this.metrics && this.metrics.outcomeCounter.labels(languageLabel, 'Success').inc();
        timer && timer();
    }
    markJ0SubmitError(languageLabel, timer) {
        if (EXPOSE_METRICS) {
            this.metrics.outcomeCounter.labels(languageLabel, 'J0-Sbmt-Err').inc();
            timer && timer();
        }
    }
    markError(languageLabel, timer) {
        if (EXPOSE_METRICS) {
            this.metrics.outcomeCounter.labels(languageLabel, 'Error').inc();
            timer && timer();
        }
    }
}

let instance = new PrometheusMetrics();

module.exports = instance;