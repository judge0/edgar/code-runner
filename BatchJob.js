let UUID = (function() {
    var self = {};
    var lut = [];
    for (var i = 0; i < 256; i++) {
        lut[i] = (i < 16 ? "0" : "") + i.toString(16);
    }
    self.generate = function() {
        var d0 = (Math.random() * 0xffffffff) | 0;
        var d1 = (Math.random() * 0xffffffff) | 0;
        var d2 = (Math.random() * 0xffffffff) | 0;
        var d3 = (Math.random() * 0xffffffff) | 0;
        return (
            lut[d0 & 0xff] +
            lut[(d0 >> 8) & 0xff] +
            lut[(d0 >> 16) & 0xff] +
            lut[(d0 >> 24) & 0xff] +
            "-" +
            lut[d1 & 0xff] +
            lut[(d1 >> 8) & 0xff] +
            "-" +
            lut[((d1 >> 16) & 0x0f) | 0x40] +
            lut[(d1 >> 24) & 0xff] +
            "-" +
            lut[(d2 & 0x3f) | 0x80] +
            lut[(d2 >> 8) & 0xff] +
            "-" +
            lut[(d2 >> 16) & 0xff] +
            lut[(d2 >> 24) & 0xff] +
            lut[d3 & 0xff] +
            lut[(d3 >> 8) & 0xff] +
            lut[(d3 >> 16) & 0xff] +
            lut[(d3 >> 24) & 0xff]
        );
    };
    return self;
})();

class BatchJob {
    // An array is used to implement priority
    constructor(
        HOSTS,
        timer,
        batchRequest,
        response,
        tokens,
        edgarInputs,
        appendResponse,
        languageLabel
    ) {
        const now = new Date();
        this.workerHost = HOSTS[Math.floor(Math.random() * HOSTS.length)];
        this.tsCreated = now;
        this.id = `${now.toLocaleDateString()} ${now.toLocaleTimeString()} ${
            this.workerHost
        }:${UUID.generate()}`;
        this.batchRequest = batchRequest;
        this.timer = timer;
        this.response = response;
        this.languageLabel = languageLabel;
        this.prometheusTimer = null;
        if (tokens && tokens.length != edgarInputs.length) {
            console.log(
                "************  Error: tokens.length != edgarInputs.length !!"
            );
        }
        this.tokens = tokens;
        this.results = edgarInputs.map((x) => {
            return {
                test_case: {
                    uuid: x.id,
                    input: x.input,
                },
                submitted: new Date(),
            };
        });
        this.appendResponse = appendResponse;
    }
    setTokens(tokens) {
        if (tokens.length != this.results.length) {
            console.log(
                "************  Error: tokens.length != edgarInputs.length !!"
            );
        }
        this.tokens = tokens;
    }
    toString() {
        return (
            "id: " +
            this.id +
            "\n languageLabel: " +
            this.languageLabel +
            "\n workerHost: " +
            this.workerHost +
            "\n tokens: " +
            JSON.stringify(this.tokens) +
            "\n results: " +
            JSON.stringify(this.results)
        );
    }
}

module.exports = BatchJob;
