const should = require('chai').should();
const request = require('supertest');
const app = require('../server.js');

describe('GET /run', () => {

    it('should not fail', (done) => {
        request(app).get('/run')
            .expect(200, done)
    });
});