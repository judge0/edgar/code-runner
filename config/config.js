const config = {
    development: require('./development-config.js'),
    production: require('./production-config.js'),
    // test: require('./test.js')
};
module.exports = config[process.env.NODE_ENV || 'development'];