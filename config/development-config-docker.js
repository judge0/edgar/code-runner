module.exports = {
    WORKER_HOSTS: [
        {
            host: "http://j0server:2358",
            weight: 1,
        },
        {
            host: "http://host:8080",
            weight: 0,
        },
    ],
    WORKER_PATH: "/submissions/batch",
    HTTP_GET_TOKEN_TIMEOUT: 5000,
    HTTP_GET_RESULT_TIMEOUT: 20000,
    BASE64_ENCODED: true,
    WAIT: false,
    CALLBACK_BASE_URL: "http://coderunner:10080",
    WAITING_ROOM_TIMEOUT: 5000,
    SUBMISSION_QUEUE_TIMEOUT: 60000,
    MAX_ACTIVE_SUBMISSIONS: 150,
    REPORT_STATS_EVERY_MINUTES: 5,
};
