module.exports = {
    WORKER_HOSTS: [
        {
            host: "http://host:port",
            weight: 20,
        },
        {
            host: "http://host:8080",
            weight: 0,
        },
    ],
    WORKER_PATH: "/submissions/batch",
    HTTP_GET_TOKEN_TIMEOUT: 5000,
    HTTP_GET_RESULT_TIMEOUT: 20000,
    BASE64_ENCODED: true,
    WAIT: false,
    CALLBACK_BASE_URL: "http://1.2.3.4:10084",
    WAITING_ROOM_TIMEOUT: 5000,
    SUBMISSION_QUEUE_TIMEOUT: 60000,
    MAX_ACTIVE_SUBMISSIONS: 150,
    REPORT_STATS_EVERY_MINUTES: 5,
};
