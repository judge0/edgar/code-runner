var winston = require('winston');
var moment = require('moment');
var osInfo = require('../common/osInfo');
let level;
let winstonFormat;
if (process.env.NODE_ENV === 'production') {
    level = 'info';
    // winstonFormat = winston.format.json();
    winstonFormat = winston.format.printf(({ level, message, timestamp, script }) => {
        return ` ${level} ${moment(timestamp).format('LTS')}: ${message}`;
    });
} else {
    level = 'debug';
    winstonFormat = winston.format.printf(({ level, message, timestamp, script }) => {
        return ` ${level} ${moment(timestamp).format('LTS')}: ${message}`;
    });
}
const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        winstonFormat
    ),
    defaultMeta: {
        username: osInfo.getUserName(),
        hostname: osInfo.getHostName(),
        script: osInfo.getScriptName(),
        appInstance: osInfo.appInstance(),
        source: 'code-runner'
    },
    transports: [
        new winston.transports.Console(),
    ]
});

logger.level = level;

module.exports = logger;
