const express = require("express");
const axios = require("axios");

let config = require("./config/config");
const winston = require("./config/winston-config");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let promMetrics = require("./PrometheusMetrics");
promMetrics.init(app);

const INSTANCE = (parseInt(process.env.pm_id, 10) || 0) + 1;

function parseHosts() {
    config.HOSTS = [];
    for (let wh of config.WORKER_HOSTS) {
        for (let i = 0; i < wh.weight; ++i) config.HOSTS.push(wh.host);
    }
    if (config.HOSTS.length === 0) {
        console.log(
            "Set worker hosts variable (WORKER_HOSTS). Current (invalid) config is:",
            config
        );
        process.exit(1);
    } else {
        console.log("Worker hosts array:", JSON.stringify(config.HOSTS));
    }
}
parseHosts();

if (!config.CALLBACK_BASE_URL) {
    console.log("Must set callback base URL (CALLBACK_BASE_URL) in config file.");
    process.exit();
}
const CALLBACK_STUB = config.CALLBACK_STUB || "callmemaybe";
const CALLBACK_URL = `${config.CALLBACK_BASE_URL}/${CALLBACK_STUB}`;

let j0LanguagesMap = new Map();
(async () => {
    const url = `${config.HOSTS[0]}/languages`;
    console.log("Getting active lanaguages list from:", url);
    const { data } = await axios.get(url, {
        timeout: config.HTTP_GET_RESULT_TIMEOUT,
        headers: config.HEADERS || {},
    });
    for (let i = 0; i < data.length; ++i) {
        console.log("Loading language info for ", data[i].id, data[i].name);
        let lang = await axios.get(`${url}/${data[i].id}`, {
            timeout: config.HTTP_GET_RESULT_TIMEOUT,
            headers: config.HEADERS || {},
        });
        j0LanguagesMap.set(data[i].id, lang.data);
    }
    console.log("Loaded " + j0LanguagesMap.size + " languages.");
})();

let JobsRepository = new (require("./JobsRepository"))(promMetrics, config);

const PORT = parseInt(process.env.PORT || 10084);

/*
		languageId = source code langugage ( 4 => C language , for others see judge0 documentation)
		base64_encoded = if true : all recived/sent string data are base64_encoded
*/
winston.info("********** Code runner judge0 defaults(params): *************");
winston.info(`BASE64_ENCODED: ${config.BASE64_ENCODED}`);
winston.info(`WAIT: false (not configurable any more...)`);

winston.info("********** Code runner config: *************");
winston.info(`WORKER_HOSTS: ${config.WORKER_HOSTS}`);
winston.info(`HOSTS array: ${config.HOSTS}`);
winston.info(`WORKER_PATH: ${config.WORKER_PATH}`);
winston.info(`HTTP_GET_TOKEN_TIMEOUT: ${config.HTTP_GET_TOKEN_TIMEOUT}`);
winston.info(`HTTP_GET_RESULT_TIMEOUT: ${config.HTTP_GET_RESULT_TIMEOUT}`);
winston.info(`INSTANCE: ${INSTANCE}`);
winston.info("********************************************");
winston.info(`WAITING_ROOM_TIMEOUT: ${config.WAITING_ROOM_TIMEOUT}`);
winston.info(`SUBMISSION_QUEUE_TIMEOUT: ${config.SUBMISSION_QUEUE_TIMEOUT}`);
winston.info("********************************************");
winston.info(`Listening on port: ${PORT}`);
winston.info(`Callback URL (mind the Firewall!): ${CALLBACK_URL}`);
if (CALLBACK_URL.indexOf(`${PORT}`) === -1) {
    winston.warn(
        `Callback URL is using different port than the one the server is listening to. This is OK if you are behind a proxy, are you?`
    );
}
winston.info("********************************************");
winston.info("Log level is: " + winston.level);
winston.info("ENV is: " + process.env.NODE_ENV);
winston.info("********************************************");
winston.info("Good luck!");

app.post("/reload", (req, res) => {
    try {
        winston.info("Reloading config...");
        delete require.cache[require.resolve("./config/config")];
        delete require.cache[require.resolve("./config/development-config")];
        delete require.cache[require.resolve("./config/production-config")];
        let newconfig = require("./config/config");
        Object.assign(config, newconfig); // keep the referece, bcs it seedes to JobsRepo and BatchJob
        parseHosts();
        winston.info("Config RELOADED:");
        winston.info(JSON.stringify(config));
        res.end("Config is now: " + JSON.stringify(config));
    } catch (error) {
        winston.error(error);
        res.end("Reload failed: " + JSON.stringify(error));
    }
});

app.get("/ping", (req, res) => {
    res.end("pong!");
});

app.get("/run", (req, res) => {
    winston.info("GET request");
    res.end("Use POST");
});

app.post("/run", async (req, res) => {
    let end, languageLabel;

    // res.status(503).json({
    //     status: {
    //         "id": 100,
    //         "description": "Pushback (service unavailable)"
    //     },
    //     error: "Pushback: request timed out in the submission room after 5000 ms.",
    // });
    // console.log("responded 503")
    // return;
    //try {
    winston.debug("Incoming request at /run");

    if (!req.body.source) {
        winston.warn("Request error: missing source code");
        return res.status(400).json({
            status: {
                id: 13,
                description: "Internal Error",
            },
            error: "Bad request: missing source code",
        });
    }
    if (!req.body.language_id || !j0LanguagesMap.has(req.body.language_id)) {
        winston.warn(
            "Request error: missing languageId or unknown language id: " +
                req.body.language_id
        );
        return res.status(400).json({
            status: {
                id: 13,
                description: "Internal Error",
            },
            error:
                "Request error: missing languageId or unknown language id: " +
                req.body.language_id,
        });
    }

    const {
        language_id: languageId,
        source,
        compiler_options,
        additional_files,
    } = req.body;
    const { runtime_constraints_question: rtcQuestion } = req.body;
    const { runtime_constraints_tests: rtcTests } = req.body;

    languageLabel = j0LanguagesMap.has(languageId)
        ? j0LanguagesMap.get(languageId).name
        : "unknown language, check your mappings!";

    promMetrics.markStartRequest(languageLabel);

    const batchRequest = getBatchRequest(
        languageId,
        compiler_options,
        source,
        rtcQuestion,
        rtcTests,
        req.body.inputs,
        additional_files
    );

    await JobsRepository.addRequest(
        res,
        batchRequest,
        req.body.inputs,
        {
            language: j0LanguagesMap.has(languageId)
                ? j0LanguagesMap.get(languageId)
                : {
                      id: 0,
                      name: "unknown language, check your mappings!",
                  },
            compiler_options: (
                j0LanguagesMap.get(languageId).compile_cmd || ""
            ).replace("%s", compiler_options),
            submitted: new Date(),
        },
        req.body.priority >= 0 ? req.body.priority : 10e6
    );

    return;

    // } catch (error) {

    //     winston.error(`Interal Error on POST (this should not happen): \n${JSON.stringify(error, null, 4)}`);

    //     const response = {
    //         status: {
    //             id: 13,
    //             description: 'Internal error',
    //         },
    //         source: req.body.source,
    //         internal_message: error.message ? error.message : error,
    //         results: [],
    //     };
    //     res.json(response);
    // }
});

app.put("/" + CALLBACK_STUB, async (req, res) => {
    try {
        let j0result = req.body;

        winston.debug("callmemaybe REQ token:" + j0result.token);

        await JobsRepository.handleJudge0Callback(j0result);

        res.status(200).send();
    } catch (error) {
        winston.error(
            `Interal Error on PUT (this should not happen): \n${JSON.stringify(
                error,
                null,
                4
            )}`
        );

        const response = {
            status: {
                id: 13,
                description: "Internal error",
            },
            source: req.body.source,
            internal_message: error.message ? error.message : error,
            results: [],
        };
        res.json(response);
    }
});

function getBatchRequest(
    languageId,
    compiler_options,
    source,
    rtcQuestion,
    rtcTests,
    inputs,
    additional_files
) {
    const codeRunnerRequests = [];
    const rtcTestsMap = {};
    (rtcTests || []).forEach((rtc) => {
        rtcTestsMap[rtc.id] = rtcTestsMap[rtc.id] || [];
        const rtcObj = {
            name: rtc.name,
            value: rtc.value,
        };
        rtcTestsMap[rtc.id].push(rtcObj);
    });

    inputs.forEach((input, index) => {
        codeRunnerRequests.push(
            prepareCodeRunnerRequest(
                source,
                languageId,
                compiler_options,
                input,
                rtcQuestion,
                rtcTestsMap[input.id], // this might be undefined, that's ok
                additional_files
            )
        );
    });
    return {
        submissions: codeRunnerRequests,
    };
}

function prepareCodeRunnerRequest(
    source,
    languageId,
    compiler_options,
    input,
    runtimeConstraints,
    runtimeConstraintsOverrides,
    additional_files
) {
    const codeRunnerRequest = {
        source_code: source,
        language_id: languageId,
        compiler_options,
        stdin: input.input,
        callback_url: CALLBACK_URL,
        additional_files,
    };

    if (runtimeConstraints && runtimeConstraints.length > 0) {
        runtimeConstraints.forEach((rtc) => {
            codeRunnerRequest[rtc.name] = rtc.value;
        });
    }

    if (runtimeConstraintsOverrides && runtimeConstraintsOverrides.length > 0) {
        runtimeConstraintsOverrides.forEach((rtc) => {
            codeRunnerRequest[rtc.name] = rtc.value;
        });
    }
    return codeRunnerRequest;
}

app.use((req, res, next) => {
    res.sendStatus(404);
});

const logErrorHandler = (err, req, res, next) => {
    winston.error(`Server error: ${JSON.stringify(err, null, 4)}`);
    console.log("Dumping request:");
    console.log("req.path = " + req.path);
    console.log("req.query = " + JSON.stringify(req.query));
    console.log("req.route = " + req.route);
    console.log("req.ip = " + req.ip);
    console.log("req.body = " + req.ip);
    console.log(req.body);
    next(err);
};

const errorHandler = (err, req, res, next) => {
    res.status(500).send(err);
};

app.use(logErrorHandler);
app.use(errorHandler);

//var bindto = process.env.NODE_BIND || 'localhost';
let bindto = "0.0.0.0"; // to enable callback
//console.log("Binding to ", bindto);

const server = app.listen(PORT, bindto, () => {
    winston.info(`Server listening on  ${bindto}:${server.address().port}`);
    winston.info(`Check your firewall (for j0 callbacks!)`);
});

module.exports = server;
