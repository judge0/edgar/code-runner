// User defined class 
// to store element and its priority 
class QElement {
    constructor(element, priority) {
        this.element = element;
        this.priority = priority;
    }
}

// PriorityQueue class 
class PriorityQueue {

    // An array is used to implement priority 
    constructor() {
        this.items = [];
    }
    // enqueue function to add element 
    // to the queue as per priority 
    add(element, priority) {
        // creating object from queue element 
        var qElement = new QElement(element, priority);
        var contain = false;

        // iterating through the entire 
        // item array to add element at the 
        // correct location of the Queue 
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].priority > qElement.priority) {
                // Once the correct location is found it is 
                // enqueued 
                this.items.splice(i, 0, qElement);
                contain = true;
                break;
            }
        }

        // if the element have the highest priority 
        // it is added at the end of the queue 
        if (!contain) {
            this.items.push(qElement);
        }
    }
    removeByElementId(id) {
        let idx = this.items.findIndex(x => x.element.id === id);        
        if (idx >= 0) {
            let item = this.items[idx].element;
            this.items.splice(idx, 1);
            return item;
        } else {
            return null;
        }
    }
    // dequeue method to remove 
    // element from the queue 
    pop() {
        if (this.isEmpty())
            return null;
        return this.items.shift().element;
    }
    // front function 
    peek() {
        // returns the highest priority element 
        // in the Priority queue without removing it. 
        if (this.isEmpty()) return null;
        return this.items[0];
    }
    // rear function 
    last() {
        // returns the lowest priorty 
        // element of the queue 
        if (this.isEmpty())
            return null;
        return this.items[this.items.length - 1];
    }
    // isEmpty function 
    isEmpty() {
        // return true if the queue is empty. 
        return this.items.length == 0;
    }
    size() {
        return this.items.length;
    }

    // printQueue function 
    // prints all the element of the queue 
    toString() {
        var str = "\n\t";
        for (var i = 0; i < this.items.length; i++)
            str += this.items[i].priority + ':\t' + this.items[i].element.id + "\n\t";
        return str;
    }
}

module.exports = PriorityQueue;