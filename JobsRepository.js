const axios = require("axios");
let BatchJob = require("./BatchJob");
let PriorityQueue = require("./PriorityQueue");
const winston = require("./config/winston-config");

const emptyStatsSlot = {
    count: 0,
    duration: 0,
    min: 1e6,
    max: 0,
    submTimeOut: 0,
    waitingRoomTimeOut: 0,
    errors: 0,
    j0submitError: 0,
};

class JobsRepository {
    constructor(promMetrics, config) {
        this.config = config;
        this.PROM_METRICS = promMetrics;
        this.submittedJobs = [];
        this.waitingRoom = new PriorityQueue();
        // Stats:
        this.submissionTimedOut = 0;
        this.waitingRoomTimedOut = 0;
        this.response200 = 0;
        this.response503 = 0;
        this.errors = 0;
        this.j0submitError = 0;
        this.stats = [];
        for (let i = 0; i < 24 * 60; ++i) {
            this.stats[i] = { ...emptyStatsSlot };
        }
        setInterval(
            () => this.garbageCollect(),
            3 * this.config.SUBMISSION_QUEUE_TIMEOUT
        );
        setInterval(
            () => this.reportStats(),
            1000 * 60 // every minute
        );
        winston.info(
            "JobsRepository initialized, garbage collection every " +
                3 * this.config.SUBMISSION_QUEUE_TIMEOUT +
                "ms."
        );
    }

    garbageCollect() {
        const now = new Date();
        const staleJobs = this.submittedJobs.filter(
            (x) => now - x.tsCreated > 2 * this.config.SUBMISSION_QUEUE_TIMEOUT
        );
        winston.info(
            `Garbage collecting (${now}): ${staleJobs.length} stale jobs.`
        );
        if (staleJobs.length) {
            winston.error(
                "#########################  Removing stale jobs:  #############################"
            );
            console.table(staleJobs);
            this.submittedJobs = this.submittedJobs.filter(
                (x) =>
                    now - x.tsCreated <=
                    2 * this.config.SUBMISSION_QUEUE_TIMEOUT
            );
            winston.error(
                "######################## After removal, active jobs left: #########################"
            );
            console.table(this.submittedJobs);
        }
    }
    reportStats() {
        const minute = this.getMinutesSinceMidnight();
        for (let i = minute + 5; i < minute + 11; ++i) {
            this.stats[i] = { ...emptyStatsSlot };
        }
        winston.debug(
            "Cleared minutes " + (minute + 5) + " to " + (minute + 10) + "."
        );
        if (minute % this.config.REPORT_STATS_EVERY_MINUTES === 0) {
            winston.info("Last hour's stats:");
            for (let i = minute; i >= 0 && i > minute - 60; --i) {
                const stat = this.stats[i];
                winston.info(
                    `[${Math.floor(i / 60)}:${i % 60 < 10 ? "0" : ""}${i %
                        60}]: runs: ${stat.count}\ttotal: ${
                        stat.duration
                    }ms\tavg: ${Math.round(
                        stat.duration / stat.count
                    )}ms, min: ${stat.min}ms\tmax: ${
                        stat.max
                    }ms\tsubmTimeOut: ${stat.submTimeOut}\twaitRoomTimeOut: ${
                        stat.waitingRoomTimeOut
                    }\terrors: ${stat.errors}\tj0submitError: ${
                        stat.j0submitError
                    }`
                );
            }
        }
    }
    getStats() {
        return (
            `*** Waiting room: ${this.waitingRoom.size()} Submission queue: ${
                this.submittedJobs.length
            } ${
                this.submittedJobs.length > this.config.MAX_ACTIVE_SUBMISSIONS
                    ? "OVFLW"
                    : ""
            }` +
            `\tTotal:\t200: ${this.response200}\t503: ${this.response503}\tSubmTimeOut: ${this.submissionTimedOut}\tWaitingRoomTimeOut: ${this.waitingRoomTimedOut}\tErrors: ${this.errors}\tj0submitError: ${this.j0submitError} ***`
        );
    }

    async addRequest(
        response,
        batchRequest,
        edgarInputs,
        appendResponse,
        priority,
        languageLabel
    ) {
        winston.info("Incoming: priority = " + priority);
        let job = new BatchJob(
            this.config.HOSTS,
            null,
            batchRequest,
            response,
            null,
            edgarInputs,
            appendResponse,
            languageLabel
        );
        if (
            priority === 0 ||
            this.submittedJobs.length < this.config.MAX_ACTIVE_SUBMISSIONS
        ) {
            job.prometheusTimer = this.PROM_METRICS.markStartRequest(
                languageLabel,
                job.workerHost
            );
            await this.addJobToSubmission(job);
        } else {
            job.prometheusTimer = this.PROM_METRICS.markStartRequest(
                languageLabel,
                job.workerHost
            );
            job.timer = setTimeout(
                () => this.timeoutWaitingRoomJob(job.id),
                this.config.WAITING_ROOM_TIMEOUT
            );
            this.waitingRoom.add(job, priority);
            winston.info(
                "!! Adding batch job to the waiting room (submitted jobs length = " +
                    this.submittedJobs.length +
                    "):" +
                    job.id
            );
            winston.info(this.getStats());
        }
    }
    async addJobToSubmission(job) {
        //console.log('console.time(' + job.id + ')');
        console.time(job.id);
        this.submittedJobs.push(job); // must go first, what follows is ASYNC code and it is important to increase the size of the array so as to avoid overflow
        winston.info(this.getStats()); // this, too
        try {
            const tokens = await this.submitCode(job);
            //console.log('--------------------- submitted -------------------------------------');
            // It's okay, job holds the reference, so the array item WILL be updated!
            job.setTokens(tokens.map((x) => x.token));
            job.timer = setTimeout(
                () => this.timeoutSubmissionJob(job.id),
                this.config.SUBMISSION_QUEUE_TIMEOUT
            );
        } catch (error) {
            //this.submittedJobs.pop();   Bugfix - this could be some other job, not the one we just added
            this.submittedJobs = this.submittedJobs.filter(
                (x) => x.id !== job.id
            );
            winston.error(
                "Failed to submit to j0 runner, removed from the submittedJobs"
            );
            job.response.status(500).json({
                status: {
                    id: 101,
                    description: "Failed to submit to j0 runner.",
                },
                error: "Failed to submit to j0 runner.",
            });
            this.PROM_METRICS.markJ0SubmitError(job);
        }
        //winston.debug("Submission room size:" + this.submittedJobs.length + " (just added " + job.id + ")");
    }

    timeoutSubmissionJob(id) {
        // find and remove, add new
        let idx = this.submittedJobs.findIndex((x) => x.id === id);
        if (idx >= 0) {
            this.submissionTimedOut++;
            winston.error(
                `!!! TIMEOUT: Submission job ${id} timed out after ${this.config.SUBMISSION_QUEUE_TIMEOUT}ms.`
            );
            winston.error(`!!! Timedout job is: ${this.submittedJobs[idx]}`);
            winston.info(this.getStats());
            this.submittedJobs[idx].response.status(503).json({
                status: {
                    id: 100,
                    description: "Pushback (service unavailable)",
                },
                error:
                    "Pushback: request timed out in the submission room after " +
                    this.config.SUBMISSION_QUEUE_TIMEOUT +
                    " ms.",
            });
            this.PROM_METRICS.markSubmissionTimeout(
                this.submittedJobs[idx].languageLabel,
                this.submittedJobs[idx].prometheusTimer
            );

            this.response503++;
            this.stats[this.getMinutesSinceMidnight()].submTimeOut++;
            this.submittedJobs.splice(idx, 1);
        } else {
            this.errors++;
            this.stats[this.getMinutesSinceMidnight()].errors++;
            winston.error(
                `!!!!!!!!!!! timeoutSubmission: could not find job with id: ${id}`
            );
            winston.error(
                `SUBMITTED:\n\t${this.submittedJobs
                    .map((x) => x.id)
                    .join("\n\t")} \nWAITROOM:${this.waitingRoom.toString()}`
            );
        }
        this.maybeTakeNext();
    }
    timeoutWaitingRoomJob(id) {
        let job = this.waitingRoom.removeByElementId(id);
        if (job) {
            this.waitingRoomTimedOut++;
            winston.info(
                `!!! TIMEOUT: Waiting room job ${id} timed out after ${this.config.WAITING_ROOM_TIMEOUT}ms.`
            );
            winston.info(this.getStats());
            job.response.status(503).json({
                status: {
                    id: 100,
                    description: "Pushback (service unavailable)",
                },
                error:
                    "Pushback: request timed out in the waiting room after " +
                    this.config.WAITING_ROOM_TIMEOUT +
                    " ms.",
            });
            this.PROM_METRICS.markWaitingRoomTimeout(
                job.languageLabel,
                job.prometheusTimer
            );
            this.stats[this.getMinutesSinceMidnight()].waitingRoomTimeOut++;
            this.response503++;
        } else {
            this.errors++;
            this.stats[this.getMinutesSinceMidnight()].errors++;
            winston.error(
                `!!!!!!!!!!! timeoutWaitingRoomJob: Could not find job with id: ${id}`
            );
            winston.error(
                `SUBMITTED:\n\t${this.submittedJobs
                    .map((x) => x.id)
                    .join("\n\t")} \nWAITROOM:${this.waitingRoom.toString()}`
            );
        }
        this.maybeTakeNext(); // this one is probably not needed
    }
    async maybeTakeNext() {
        winston.info(this.getStats());
        if (
            !this.waitingRoom.isEmpty() &&
            this.submittedJobs.length < this.config.MAX_ACTIVE_SUBMISSIONS
        ) {
            let job = this.waitingRoom.pop();
            clearTimeout(job.timer);
            await this.addJobToSubmission(job); // will print stats here..
        }
    }

    async handleJudge0Callback(j0result) {
        let job;
        try {
            let found = 0;
            let jobIndex;
            for (let i = 0; i < this.submittedJobs.length; ++i) {
                job = this.submittedJobs[i];
                let idx =
                    job.tokens && job.tokens.length
                        ? job.tokens.indexOf(j0result.token)
                        : -1;
                if (idx >= 0) {
                    jobIndex = i;
                    if (job.results[idx].status) {
                        winston.error(
                            "Warining: j0 double reporting !?  Ignored."
                        );
                    } else {
                        job.results[idx] = { ...job.results[idx], ...j0result };
                        job.results[idx].finished = new Date();
                    }
                    found = 1;
                    break;
                }
            }
            if (!found) {
                winston.info(
                    "Warning: Unknown job(submission): " +
                        JSON.stringify(j0result)
                );
                //winston.error("submittedJobs is: " + this.submittedJobs.map(x => x.toString()).join("\n"));
            } else {
                if (
                    job.results
                        .map((x) => (x.status ? 1 : 0))
                        .reduce((a, b) => a + b, 0) === job.results.length
                ) {
                    clearTimeout(job.timer);
                    let response = {
                        ...{
                            status: {
                                id: 14,
                                description: "Finished",
                            },
                            results: job.results,
                        },
                        ...job.appendResponse,
                        finished: new Date(),
                    };
                    this.PROM_METRICS.markSuccess(
                        job.languageLabel,
                        job.prometheusTimer
                    );
                    let stat = this.stats[
                        this.getMinutesSinceMidnight(job.tsCreated)
                    ];
                    stat.count++;
                    const duration = new Date() - job.tsCreated;
                    stat.duration += duration;
                    if (stat.min > duration) stat.min = duration;
                    if (stat.max < duration) stat.max = duration;

                    //console.log('console.timeEnd(' + job.id + ')');
                    console.timeEnd(job.id);
                    // winston.debug("Responding, yay !!");
                    this.response200++;
                    // console.log(JSON.stringify(response, null, 2));
                    job.response.json(response);
                    this.submittedJobs.splice(jobIndex, 1);
                    await this.maybeTakeNext();
                }
            }
        } catch (error) {
            winston.error("handleJudge0Callback error!");
            winston.error(error);
            if (job && job.languageLabel && job.timer) {
                this.PROM_METRICS.markError(job.languageLabel, job.timer);
            }
        }
    }
    getMinutesSinceMidnight(now) {
        if (!now) now = new Date();
        return now.getHours() * 60 + now.getMinutes();
    }
    async submitCode(job) {
        try {
            const submitURL =
                job.workerHost +
                this.config.WORKER_PATH +
                `?base64_encoded=${this.config.BASE64_ENCODED}&wait=false`;

            winston.info("--------------------> submitting to: " + submitURL);
            winston.debug(JSON.stringify(job.batchRequest.submissions[0]));
            const batchResponse = (await axios.post(
                submitURL,
                job.batchRequest,
                {
                    timeout: this.config.HTTP_GET_TOKEN_TIMEOUT,
                    headers: this.config.HEADERS || {}
                }
            )).data;
            //console.log("Submitted " + job.id + " to:" + submitURL + " tokens.len = " + batchResponse.length);
            //winston.debug('Submission reponse: \n' + JSON.stringify(batchResponse, null, 4));
            return batchResponse;
        } catch (error) {
            this.j0submitError++;
            this.stats[this.getMinutesSinceMidnight()].errors++;
            winston.error(
                `submitCode problem with submission POST request: \n${JSON.stringify(
                    error,
                    null,
                    4
                )}`
            );
            throw error;
        }
    }
}

module.exports = JobsRepository;
