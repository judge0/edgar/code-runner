# Code-runner

Code-runner is an arbitrary programming language runner for Edgar.  
It wraps the [Judge0 API](https://judge0.com/), which in turn wraps [isolate](https://github.com/ioi/isolate).  
This project is needed if you want to run C, C#, C++, Java, Python, etc. questions in Edgar.

# Setup:

 * Install latest Node.js
 * `git@gitlab.com:edgar-group/code-runner.git`
 * `npm install`
 * Create files (by copying and adjusting `config/development-config-TEMPLATE.js`):
   * `config/development-config.js`
   * `config/production-config.js`
 * Run code-runner with: `npm start`
 