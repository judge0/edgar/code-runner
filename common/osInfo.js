const os = require('os');
var path = require('path');

var username = os.userInfo().username;
var hostname = String(os.hostname);
var rootPath = path.resolve(__dirname);
rootPath = rootPath.substring(0, rootPath.indexOf("code-runner") + 12);
var instance = process.env.NODE_APP_INSTANCE ? parseInt(process.env.NODE_APP_INSTANCE) : 0;

const getUserName = () => {
    return username;
}
const getHostName = () => {
    return hostname;
}
const getScriptName = () => {
    var originalFunc = Error.prepareStackTrace;

    var callerfile;
    try {
        var err = new Error();
        var currentfile;

        Error.prepareStackTrace = function (err, stack) { return stack; };

        currentfile = err.stack.shift().getFileName();

        while (err.stack.length) {
            callerfile = err.stack.shift().getFileName();

            if(currentfile !== callerfile) break;
        }
    } catch (e) {}

    Error.prepareStackTrace = originalFunc; 
    
    return callerfile.replace(rootPath, '');
}
const appInstance = () => {
    return instance;
}

module.exports = {
    getUserName: getUserName,
    getHostName: getHostName,
    getScriptName: getScriptName,
    appInstance: appInstance
};
